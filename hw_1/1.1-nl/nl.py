from sys import argv, stdin, stderr


if __name__ == '__main__':
    if len(argv) > 2:
        print('USAGE: nl [input file]', file=stderr)
        exit(1)

    c = 1

    if len(argv) == 1:
        for s in stdin:
            print(f'{c} '.rjust(7), s, end='')
            c += 1
    else:
        with open(argv[1], 'r') as input_file:
            for line in input_file:
                print(f'{c} '.rjust(7), line, end='')
                c += 1
