from collections import deque
from typing import List, TextIO
from sys import argv, stdin


def last_n_lines(fp: TextIO, n: int) -> List[str]:
    buf = deque(maxlen=n)
    for line in fp:
        buf.append(line)

    return list(buf)


if __name__ == '__main__':
    if len(argv) >= 2:
        files = argv[1:]

        if len(files) == 1:
            print_fname = False
        else:
            print_fname = True

        for file in files:
            with open(file, 'r') as f:
                if print_fname:
                    print('==>', file, '<==')

                print(*last_n_lines(f, 10), sep='', end='')
    else:
        print(*last_n_lines(stdin, 17), sep='', end='')
