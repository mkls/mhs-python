from sys import argv, stdin
from typing import TextIO
from string import whitespace

BUF_SIZE = 1024


def count_stats(fd: TextIO) -> (int, int, int):
    n_bytes = 0
    n_newlines = 0
    n_words = 0

    prev_ch = None

    while len(buf := fd.read(BUF_SIZE)) > 0:
        for ch in buf:
            if ch in whitespace:
                if prev_ch not in whitespace:
                    n_words += 1

                if ch == '\n':
                    n_newlines += 1

            n_bytes += len(ch.encode())
            prev_ch = ch

    return n_bytes, n_newlines, n_words


def pretty_print(n_bytes: int, n_newlines: int,
                 n_words: int, file: str | None) -> None:
    res_str = ' '.join([
        str(n_newlines).rjust(4),
        str(n_words).rjust(4),
        str(n_bytes).rjust(4)]
    )

    if file:
        res_str += ' ' + file

    print(res_str)


if __name__ == '__main__':
    n_bytes, n_newlines, n_words = (0, 0, 0)

    if len(argv) == 1:
        b, n, w = count_stats(stdin)
        pretty_print(b, n, w, None)

    for file in argv[1:]:
        try:
            with open(file, 'r') as f:
                b, n, w = count_stats(f)

            pretty_print(b, n, w, file)

            n_bytes += b
            n_newlines += n
            n_words += w
        except (IOError, OSError, RuntimeError) as e:
            print(f'Failed reading {file}: {e}')

    if len(argv) > 2:
        pretty_print(n_bytes, n_newlines, n_words, 'total')
