~/c/p/1.1-nl (main↑1|✔) $ python ./nl.py ./nl.py
     1  from sys import argv, stdin, stderr
     2
     3
     4  if __name__ == '__main__':
     5      if len(argv) > 2:
     6          print('USAGE: nl [input file]', file=stderr)
     7          exit(1)
     8
     9      c = 1
    10
    11      if len(argv) == 1:
    12          for s in stdin:
    13              print(f'{c} '.rjust(7), s, end='')
    14              c += 1
    15      else:
    16          with open(argv[1], 'r') as input_file:
    17              for line in input_file:
    18                  print(f'{c} '.rjust(7), line, end='')
    19                  c += 1
~/c/p/1.1-nl (main↑1|✔) $ nl -b a ./nl.py
     1  from sys import argv, stdin, stderr
     2
     3
     4  if __name__ == '__main__':
     5      if len(argv) > 2:
     6          print('USAGE: nl [input file]', file=stderr)
     7          exit(1)
     8
     9      c = 1
    10
    11      if len(argv) == 1:
    12          for s in stdin:
    13              print(f'{c} '.rjust(7), s, end='')
    14              c += 1
    15      else:
    16          with open(argv[1], 'r') as input_file:
    17              for line in input_file:
    18                  print(f'{c} '.rjust(7), line, end='')
    19                  c += 1
