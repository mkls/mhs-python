# Extremely basic TeX table generator

This package implements `lists_to_table()` func that generates
LaTeX table from list of lists of data:
```python
import gen_table

data = [[1, 2, 3], [4, 5, 6]]
print(gen_table.lists_to_table(data))

# Prints
# \begin{tabular}{|c|c|c|}
# \hline
#         1 & 2 & 3 \\
#         4 & 5 & 6 \\
# \hline
# \end{tabular}
```

## Note
Unlike Gitlab, GitHub still does not support hosting python packages, so for the
sake of simplicity it is added to git as an LFS file.

## Build
Install `build` package:
`pip install build` (or use package from your distribution)

Build the table generator:
`python -m build`

Obtain the resulting wheel and sdist from `dist/` subdir
