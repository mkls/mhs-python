from sys import stderr
from typing import Any, List
from functools import reduce


def _gen_table_data(data: List[List[Any]]) -> str:
    return reduce(lambda prev, x: prev + '\t' + x + ' \\\\\n',
                  map(_gen_table_line, data),
                  '')


def _gen_table_line(line: List[Any]) -> str:
    return ' & '.join(map(str, line))


def _wrap_hlines(str_data: str) -> str:
    return '\\hline\n' + str_data + '\\hline\n'


def lists_to_table(data: List[List[Any]]) -> str:
    begin = r'\begin{tabular}'
    end = r'\end{tabular}'

    columns = '|c' * len(data[0]) + '|'
    str_data = _wrap_hlines(_gen_table_data(data))

    return begin + '{' + columns + '}\n' + str_data + end


if __name__ == '__main__':
    n = int(input('Enter the number of table rows: '))

    if n <= 0:
        print('n must be > 0', file=stderr)
        exit(1)

    print('Enter the rows:')

    data = []

    for i in range(n):
        try:
            data.append(input().split())
        except EOFError:
            print('Line cannot be empty', file=stderr)
            exit(1)

    try:
        with open('table.tex', 'w') as f:
            f.write(lists_to_table(data) + '\n')
    # TODO: handle only specific exception types
    except Exception as e:
        print('Failed to generate `table.tex`:', e, file=stderr)
        exit(1)
