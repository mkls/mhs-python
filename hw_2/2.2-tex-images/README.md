# Generate simple TeX doc with table and image
## Install dependencies
```
$ python -m venv venv
$ . venv/bin/activate
$ pip install -r requirements.txt.lock \ 
    --index-url https://gitlab.com/api/v4/projects/53120827/packages/pypi/simple
```

## Run script
```
$ python ./src/gen-tex-doc/gen_doc.py python-logo
```

**Note**: `python-logo` in example is placed under `images` directory.
