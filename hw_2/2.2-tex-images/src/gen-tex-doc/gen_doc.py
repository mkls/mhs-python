import os.path as op

from typing import Iterable
from gen_table import lists_to_table
from sys import argv, stderr


def _img(path: str) -> str:
    img = r'\includegraphics[width=\textwidth]{%path%}'
    img = img.replace('%path%', path)
    return img


def get_doc(contents: Iterable[str]) -> str:
    path = op.dirname(op.realpath(__file__))
    path = op.join(path, 'template.tex')

    with open(path, 'r') as f:
        template = f.read()

    c = '\n'.join(contents)
    return template.replace('%content%', c)


if __name__ == '__main__':
    if len(argv) != 2:
        print('USAGE: ./gen_doc.py image_name', file=stderr)
        exit(1)

    data = [[1, 2, 3.5], [4.5, 123, '-']]
    table = lists_to_table(data)
    img = _img(argv[1])

    with open('result.tex', 'w') as f:
        f.write(get_doc([img, table]))
