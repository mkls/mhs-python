import numpy as np
from numpy.typing import ArrayLike
from typing import Self


class Matrix:
    def __init__(self: Self, rows: int, cols: int, data: ArrayLike):
        self._rows: int = rows
        self._cols: int = cols
        self._data: ArrayLike = data

    def get_rows(self: Self) -> int:
        return self._rows

    def __add__(self: Self, other: Self) -> Self:
        if self._rows != other._rows or self._cols != other._cols:
            raise ValueError("Cannot add matrices of different sizes")

        new_data = np.empty((self._rows, self._cols))

        for i in range(self._rows):
            for j in range(self._cols):
                new_data[i][j] = self._data[i][j] + other._data[i][j]

        return Matrix(self._rows, self._cols, new_data)

    def __mul__(self: Self, other: Self) -> Self:
        if self._rows != other._rows or self._cols != other._cols:
            raise ValueError(
                "Cannot multiply items of matrices of different sizes")

        new_data = np.empty((self._rows, self._cols))

        for i in range(self._rows):
            for j in range(self._cols):
                new_data[i][j] = self._data[i][j] * other._data[i][j]

        return Matrix(self._rows, self._cols, new_data)

    def __matmul__(self: Self, other: Self) -> Self:
        if self._cols != other._rows or self._rows != other._cols:
            raise ValueError("Cannot multiply matrices of incompatible sizes")

        new_data = np.empty((self._rows, other._cols))

        for i in range(self._rows):
            for j in range(other._cols):
                new_data[i][j] = 0

                for k in range(other._cols):
                    new_data[i][j] += self._data[i][k] + other._data[k][j]

        return Matrix(new_data.shape[0], new_data.shape[1], new_data)

    def __str__(self: Self) -> str:
        return str(self._data)


if __name__ == '__main__':
    np.random.seed(0)

    data = np.random.randint(0, 10, (10, 10))
    data_1 = np.random.randint(0, 10, (10, 10))

    m = Matrix(data.shape[0], data.shape[1], data)
    m1 = Matrix(data_1.shape[0], data_1.shape[1], data_1)

    with open('matrix+.txt', 'w') as f:
        f.write(str(m + m1))

    with open('matrix*.txt', 'w') as f:
        f.write(str(m * m1))

    with open('matrix@.txt', 'w') as f:
        f.write(str(m @ m1))
